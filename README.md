---
description: >-
  Smartcloud Artificial Assistant is a software developed by TGT Media Ltd for
  the open-source community. Designed to be extremely easy to modify and
  generate new content around.
---

# Smartcloud Artificial Assistant \(SCAA\)

![](https://github.com/TGTMedia/smartcloud_wiki/blob/0.0.0/.gitbook/assets/logo.png)

#### What is Smartcloud Artificial Assistant?

Smartcloud Artificial Assistant otherwise known as SCAA, is an open-source software for integrating applications and software, designed for small communities, it has been built for simplicity.

#### Why was it created?

When this project started, it was not much more than a discord application communicating between multiple end-points to run community focused actions and operations. Now growing to become much more than a simple bot, this application has been rewritten to be modular and made open source.

#### Why use it over other options?

We are aware of other options which you could use instead of our application, such as xxx and yyyy, however we believe, that our system with the support of our community could be more powerful and easier to use.

#### I'm not a developer, can I use SCAA?

Yes, this project is developed so that all level of users can use it, with versions pre-packaged as well as simple CLI tools, we believe we have created the simplest project to use for users who want to create custom applications.

We also have started work on an hosted version, with more features prebuilt into the application. This allows our users to experience Smartcloud at it's full potential without having complex setups.

#### Where is the official support & documentation?

All our official documentation is powered by Gitbook and can be located within the `@smartcloud/app/wiki` directory or can be found online at https://documentation.scaa.com/

## Getting Started

### Installation

#### Automatic installation

We have built a automatic installation system to ensure speed and simplicity. This installer will guide you through all the options of SCAA, and automatically install all dependencies, modules, templates and much more.

Simply install our command line interface using:

```bash
npm i -g @smartcloud/cli
```

Once installed, run the command

```bash
smartcloud install
```

You can read more about our automatic installation options here:

{% page-ref page="packages/installation/" %}

#### Manual installation

{% hint style="danger" %}
_**HIGHLY NOT RECOMMENDED - THIS TAKES 10X LONGER TO COMPLETE**_
{% endhint %}

We haven't actually written this section yet, please come back soon...

### Contributing

{% page-ref page="getting-started/contributing/" %}

### Licensing

We originally designed this application to be completely closed-source, therefore it was built with a license system integrated. When we decided to go open-source, rather then delete the license system, we simply made the license free to purchase and added a signup into our installer, while this license exists, it is purely used to track users, provide support and operational assistance to users who need it.

{% hint style="danger" %}
However, with this said, users who choose not to register a license with us, will not receive support outside our forums and Gitlab support pages.
{% endhint %}
